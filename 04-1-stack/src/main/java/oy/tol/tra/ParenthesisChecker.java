package oy.tol.tra;
public class ParenthesisChecker {

   private static final ParenthesesException invalidOrderException = new ParenthesesException("Invalid order of parenthesis in file", ParenthesesException.PARENTHESES_IN_WRONG_ORDER);
   private static final ParenthesesException tooManyClosingParenthesesException = new ParenthesesException("Invalid parenthesis in file", ParenthesesException.TOO_MANY_CLOSING_PARENTHESES);
   private static final ParenthesesException tooFewClosingP = new ParenthesesException("Invalid parenthesis in file", ParenthesesException.TOO_FEW_CLOSING_PARENTHESES);
   
   private ParenthesisChecker() {
   }

   /**
    * Function which checks if the given string has matching opening and closing
    * parentheses. Checks for matching parentheses:<p>
    *  <code>Lorem ipsum ( dolor sit {  amet, [ consectetur adipiscing ] elit, sed } do eiusmod tempor ) incididunt ut...</code>,
    * <p>
    * which can be found for example in Java source code and JSON structures.
    * <p>
    * If the string has issues with parentheses, throws a {@code ParenthesisException} with a
    * descriptive message and error code.
    * <p>
    * <ul>
    *  <li>when an opening parenthesis is found in the string, it is successfully pushed to the stack.
    *  <li>when a closing parenthesis is found in the string, a matching opening parenthesis is popped from the stack.
    *  <li>when popping a parenthesis from the stack, it must not be null. Otherwise string has too many closing parentheses.
    *  <li>when the string has been handled, and if the stack still has parentheses, there are too few closing parentheses.
    * </ul>
    * @param stack The stack object used in checking the parentheses from the string.
    * @param fromString A string containing parentheses to check if it is valid.
    * @return Returns the number of parentheses found from the input in total (both opening and closing).
    * @throws ParenthesesException if the parentheses did not match as intended.
    * @throws StackAllocationException If the stack cannot be allocated or reallocated if necessary.
    */
    public static int checkParentheses(StackInterface<Character> stack, String fromString)
    throws ParenthesesException, StackAllocationException, StackIsEmptyException {
int count = 0;
int index = 0;

while (index < fromString.length()) {
    char c = fromString.charAt(index);
    if (c == '(' || c == '[' || c == '{') {
        stack.push(c);
        count++;
    } else if (c == ')' || c == ']' || c == '}') {
        count++;
        try {
            char pop = stack.pop();
            if ((c == ')' && pop != '(') || (c == ']' && pop != '[') || (c == '}' && pop != '{')) {
                throw invalidOrderException;
            }
        } catch (StackIsEmptyException e) {
            throw tooManyClosingParenthesesException;
        }
    }
    index++;
}

if (!stack.isEmpty()) {
    throw tooFewClosingP;
}

return count;
}
}
