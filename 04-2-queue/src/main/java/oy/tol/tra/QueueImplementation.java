package oy.tol.tra;

import java.util.Queue;

public class QueueImplementation<E> implements QueueInterface<E> {

    private Object[] itemArray;
    private int capacity;
    private static final int MY_DEFAULT_CAPACITY = 10;
    private int head = 0;
    private int tail = 0;

    public QueueImplementation() throws QueueAllocationException {
        this(MY_DEFAULT_CAPACITY);
    }

    public QueueImplementation(int capacity) throws QueueAllocationException {
        if (capacity < 2) {
            throw new QueueAllocationException("capacity < 2");
        }
        this.capacity = capacity;
        try {
            itemArray = new Object[capacity];
        } catch (Exception e) {
            throw new QueueAllocationException("Error allocating queue array");
        }
    }

    /**
     * Returns the number of elements the queue can currently hold.
     *
     * @return The current capacity of the queue.
     */
    @Override
    public int capacity() {
        return capacity;
    }

    /**
     * Adds a non-null element to the queue.
     *
     * @param element The element to add.
     * @throws NullPointerException      If the element is null.
     * @throws QueueAllocationException If reallocation fails when needed.
     */
    @Override
    public void enqueue(E element) throws NullPointerException, QueueAllocationException {
        if (element == null) {
            throw new NullPointerException("null element");
        }
        if (tail == capacity) {
            try {
                capacity *= 2;
                Object[] newItemArray = new Object[capacity];
                int i = head;
                while (i < tail) {
                    newItemArray[i] = itemArray[i];
                    i++;
                }
                itemArray = newItemArray;
            } catch (Exception e) {
                throw new QueueAllocationException("Failed to allocate a new array");
            }
        }
        itemArray[tail] = element;
        tail++;
    }

    /**
     * Removes and returns the element from the head of the queue.
     *
     * @return The element at the head of the queue.
     * @throws QueueIsEmptyException If the queue is empty.
     */
    @Override
    public E dequeue() throws QueueIsEmptyException {
        if (isEmpty()) {
            throw new QueueIsEmptyException("Queue is empty");
        }
        head++;
        return (E) itemArray[head - 1];
    }

    /**
     * Returns the element at the head of the queue without removing it.
     *
     * @return The element at the head of the queue.
     * @throws QueueIsEmptyException If the queue is empty.
     */
    @Override
    public E element() throws QueueIsEmptyException {
        if (isEmpty()) {
            throw new QueueIsEmptyException("Queue is empty");
        }
        return (E) itemArray[head];
    }

    /**
     * Returns the count of elements currently in the queue.
     *
     * @return The number of elements in the queue.
     */
    @Override
    public int size() {
        return (tail - head);
    }

    /**
     * Checks if the queue is empty.
     *
     * @return True if the queue is empty, false otherwise.
     */
    @Override
    public boolean isEmpty() {
        return (head == tail);
    }

    /**
     * Resets the queue to an empty state, removing all objects.
     * The capacity remains unchanged.
     */
    @Override
    public void clear() {
        head = 0;
        tail = 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        int i = head;
        while (i < tail) {
            sb.append(itemArray[i]);
            if (i != tail - 1) {
                sb.append(", ");
            }
            i++;
        }
        sb.append("]");
        return sb.toString();
    }
}
