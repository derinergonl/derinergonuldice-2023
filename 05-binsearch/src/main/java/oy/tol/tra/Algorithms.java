package oy.tol.tra;

import java.util.function.Predicate;

public class Algorithms {

    // Swaps two different elements in the array.
    public static <T> void swap(T[] array, int i, int j) {
        T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    // Sorts the array by comparing two elements at a time.
    // The compareTo method returns a positive number if the first element is greater,
    // a negative number if the second element is greater, and 0 if they are equal.
    // If the comparison result is positive, the elements are swapped.
    public static <T extends Comparable<T>> void sort(T[] array) {
        int i = 0;

        while (i < array.length - 1) {
            int j = 0;

            while (j < array.length - 1 - i) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    swap(array, j, j + 1);
                }
                j++;
            }
            i++;
        }
    }

    // Reverses the order of elements in the array.
    public static <T> void reverse(T[] array) {
        int i = 0;
        int j = array.length - 1;

        while (i < j) {
            swap(array, i, j);
            i++;
            j--;
        }
    }

    // Finds the mode (most frequent element) in the array.
    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T[] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();

        if (array != null && array.length > 1) {
            sort(array);
            T currentMode = array[0];
            T compareMode = null;
            int countA = 1;
            int countB = 0;

            for (int i = 1; i < array.length; i++) {
                if (array[i].equals(array[i - 1])) {
                    countA++;
                } else {
                    if (countA > countB) {
                        compareMode = currentMode;
                        countB = countA;
                    }
                    currentMode = array[i];
                    countA = 1;
                }
            }
            if (countA > countB) {
                compareMode = currentMode;
                countB = countA;
            }
            result.theMode = compareMode;
            result.count = countB;

        } else {
            result.theMode = null;
            result.count = -1;
        }
        return result;
    }

    // Partitions the array based on a specified rule.
    public static <T> int partitionByRule(T[] array, int count, Predicate<T> rule) {
        int i = 0;

        while (i < count && !rule.test(array[i])) {
            i++;
        }
        if (i >= count) {
            return count;
        }

        int j = i + 1;

        while (j < count) {
            if (!rule.test(array[j])) {
                swap(array, i, j);
                i++;
            }
            j++;
        }
        return i;
    }

    // Performs a binary search on the array.
    public static <T extends Comparable<T>> int binarySearch(T aValue, T[] fromArray, int fromIndex, int toIndex) {
        while (fromIndex <= toIndex) {
            int mid = (fromIndex + toIndex) / 2;
            int comparisonResult = aValue.compareTo(fromArray[mid]);
            if (comparisonResult == 0) {
                return mid;
            } else if (comparisonResult > 0) {
                fromIndex = mid + 1;
            } else {
                toIndex = mid - 1;
            }
        }
        return -1;
    }

    // Container class for mode search results.
    public static class ModeSearchResult<T> {
        public T theMode;
        public int count = 0;
    }
}
