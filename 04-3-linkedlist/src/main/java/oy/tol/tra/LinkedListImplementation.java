package oy.tol.tra;

public class LinkedListImplementation<E> implements LinkedListInterface<E> {

   // Node class for holding data in the linked list
   private class Node<T> {
      Node(T data) {
         element = data;
         next = null;
      }
      T element;
      Node<T> next;
      // Override toString for better string representation
      @Override
      public String toString() {
         return element.toString();
      }
   }

   // Head of the linked list and size of the list
   private Node<E> head = null;
   private int size = 0;

   // Add an element to the end of the list
   @Override
   public void add(E element) throws NullPointerException, LinkedListAllocationException {
      if (element == null) {
         throw new LinkedListAllocationException("Cannot add null to the list");
      }

      Node<E> addedNode = new Node<>(element);

      if (head == null) {
         head = addedNode;
      } else {
         Node<E> currentNode = head;
         // Traverse to the end of the list
         while (currentNode.next != null) {
            currentNode = currentNode.next;
         }
         currentNode.next = addedNode;
      }

      size++;
   }

   // Add an element at a specified index in the list
   @Override
   public void add(int index, E element) throws NullPointerException, LinkedListAllocationException, IndexOutOfBoundsException {
      if (element == null) {
         throw new LinkedListAllocationException("Cannot add null to list");
      }

      if (index < 0 || index > size) {
         throw new IndexOutOfBoundsException("Index out of bounds for adding");
      }

      Node<E> addedNode = new Node<>(element);

      if (index == 0) {
         addedNode.next = head;
         head = addedNode;
      } else {
         Node<E> currentNode = head;
         int i = 0;
         // Traverse to the node before the specified index
         while (i < index - 1) {
            currentNode = currentNode.next;
            i++;
         }

         addedNode.next = currentNode.next;
         currentNode.next = addedNode;
      }

      size++;
   }

   // Remove the first occurrence of an element from the list
   @Override
   public boolean remove(E element) throws NullPointerException {
      if (element == null) {
         throw new NullPointerException("Cannot remove null from the list");
      }

      if (head == null) {
         return false;
      }

      if (head.element.equals(element)) {
         head = head.next;
         size--;
         return true;
      }

      Node<E> currentNode = head;
      // Traverse to the node before the target element
      while (currentNode.next != null && !currentNode.next.element.equals(element)) {
         currentNode = currentNode.next;
      }

      if (currentNode.next != null) {
         currentNode.next = currentNode.next.next;
         size--;
         return true;
      }

      return false;
   }

   // Remove an element at a specified index from the list
   @Override
   public E remove(int index) throws IndexOutOfBoundsException {
      if (index < 0 || index >= size) {
         throw new IndexOutOfBoundsException("Index out of bounds for removing");
      }

      E removedElement;

      if (index == 0) {
         removedElement = head.element;
         head = head.next;
      } else {
         Node<E> currentNode = head;
         int i = 0;
         // Traverse to the node before the specified index
         while (i < index - 1) {
            currentNode = currentNode.next;
            i++;
         }
         removedElement = currentNode.next.element;
         currentNode.next = currentNode.next.next;
      }

      size--;
      return removedElement;
   }

   // Get an element at a specified index in the list
   @Override
   public E get(int index) throws IndexOutOfBoundsException {
      if (index < 0 || index >= size) {
         throw new IndexOutOfBoundsException("Index out of bounds");
      }

      Node<E> currentNode = head;
      int i = 0;
      // Traverse to the specified index
      while (i < index) {
         currentNode = currentNode.next;
         i++;
      }

      return currentNode.element;
   }

   // Find the index of the first occurrence of an element in the list
   @Override
   public int indexOf(E element) throws NullPointerException {
      if (element == null) {
         throw new NullPointerException("Cannot search for null in the list");
      }

      Node<E> currentNode = head;
      int index = 0;

      // Traverse until the target element is found
      while (currentNode != null && !currentNode.element.equals(element)) {
         currentNode = currentNode.next;
         index++;
      }

      return (currentNode == null) ? -1 : index;
   }

   // Get the size of the list
   @Override
   public int size() {
      return size;
   }

   // Clear the list
   @Override
   public void clear() {
      head = null;
      size = 0;
   }

   // Reverse the order of elements in the list
   @Override
   public void reverse() {
      if (head == null || head.next == null) {
         return;
      }

      Node<E> prevNode = null;
      Node<E> currentNode = head;
      Node<E> nextNode = null;

      // Reverse the list by adjusting pointers
      while (currentNode != null) {
         nextNode = currentNode.next;
         currentNode.next = prevNode;
         prevNode = currentNode;
         currentNode = nextNode;
      }

      head = prevNode;
   }

   // Generate a string representation of the list
   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder("[");
      Node<E> currentNode = head;
      // Traverse the list and append elements to the string
      while (currentNode != null) {
         sb.append(currentNode.element);
         if (currentNode.next != null) {
            sb.append(", ");
         }
         currentNode = currentNode.next;
      }
      sb.append("]");
      return sb.toString();
   }
}
