package oy.tol.tra;

public class Algorithms {

   public static <T extends Comparable<T>> void sort(T[] array) {
      int i = 0;
      while (i < array.length - 1) {
         int j = 0;
         while (j < array.length - i - 1) {
            if (array[j].compareTo(array[j + 1]) > 0) {
               swap(array, j, j + 1);
            }
            j++;
         }
         i++;
      }
   }

   public static <T> void reverse(T[] array) {
      int i = 0;
      int j = array.length - 1;
      while (i < j) {
         swap(array, i, j);
         i++;
         j--;
      }
   }

   public static <T> void swap(T[] array, int i, int j) {
      T temp = array[i];
      array[i] = array[j];
      array[j] = temp;
   }
}
