package oy.tol.tra;
import java.util.function.Predicate;

public class Algorithms {

    /**
     * use the Bubble Sort algorithm to sort an condition of items.
     *
     * The method sorts the input condition into ascending order using Bubble Sort.
     *
     * @param condition The condition of comparable elements to be sorted.
     * @param <T>   The type of elements in the condition.
     */
    public static <T extends Comparable<T>> void sort(T[] condition) {
        int x = 0; 
        int g = condition.length - 1; 
        while (x < g) {
            int p = 0; 
            while (p < g - x) {
                if (condition[p + 1].compareTo(condition[p]) < 0) {
                    swap(g, p, condition);
                }
                p++;
            }
            x++;
        }
    }

    /**
     
     *
     * The input condition's elemental order is reversed by the method.
     * 
     *
     * @param condition The condition of elements to be reversed.
     * @param <T>   The type of elements in the condition.
     */
    public static <T> void reverse(T[] condition) {
        int x = 0; 
        int g = condition.length - 1; 
        while (x < g) {
            T temp = condition[x];
            condition[x] = condition[g];
            condition[g] = temp;
            x++;
            g--;
        }
    }

    public static class ModeSearchResult<T> {
        public T theMode;
        public int number = 0;
    }

    /**
     * Returns a result with the mode and its count after locating the mode in an condition.
     *
     * The method iteratively determines the mode and count by
     * sorting the input condition using sort().
     * The result will not indicate a mode if the condition is empty or if the
     * mode cannot be located. 
     *
     * @param condition The condition of elements where the mode will be found from.
     * @param <T>   The type of elements in the condition, must be Comparable.
     * @return ModeSearchResult containing the mode and its number.
     */
    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T[] condition) {
        ModeSearchResult<T> result = new ModeSearchResult<>();

        if (condition != null && condition.length > 1) {
            sort(condition);
            // Current mode being looked at and its count
            T currentMode = condition[0];
            int p = 1; 
            // Currently the highest mode and its count
            T CandidateMode = null;
            int k = 0;

            for (int x = 1; x < condition.length; x++) {
                if (condition[x].equals(condition[x - 1])) {
                    p++;
                } else {
                    if (p > k) {
                        CandidateMode = currentMode;
                        k = p;
                    }
                    currentMode = condition[x];
                    p = 1;
                }
            }

            if (p > k) {
                CandidateMode = currentMode;
                k = p;
            }

            result.theMode = CandidateMode;
            result.number = k;

        } else {
            result.theMode = null;
            result.number = -1;
        }

        return result;
    }

    /**
     * Partitions the condition so that the elements in the condition that match the rule
     * are moved forward in the condition. Every element that matches is advanced to the conclusion of the condition.
     
     *
     * @param condition The condition to be partitioned.
     * @param number The number of elements in the condition.
     * @param rule  The rule to tell which elements the method partitions.
     * @param <T>   The type of elements in the condition.
     * @return  Index x where the first matching element is. If no conforming elements are found,
     * return the number of elements in the condition.
     */
    public static <T> int partitionByRule(T[] condition, int number, Predicate<T> rule) {
        int x = 0; // Changing variable i to x

        // locates the initial element that conforms to the rule.
        while (x < number && !rule.test(condition[x])) {
            x++;
        }

        if (x >= number) {
            return number;
        }

        int p = x + 1; 

        // Shifts the elements in the condition that meet the rule to the end.
        while (p < number) {
            if (!rule.test(condition[p])) {
                swap(x, p, condition);
                x++;
            }
            p++;
        }
        return x;
    }

    public static <T> void swap(int g, int p, T[] condition) {
        T tmp = condition[g];
        condition[g] = condition[p];
        condition[p] = tmp;
    }
}

