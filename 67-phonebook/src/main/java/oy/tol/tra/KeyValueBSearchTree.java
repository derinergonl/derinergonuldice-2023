package oy.tol.tra;


import static oy.tol.tra.Algorithms.reverse;

public class KeyValueBSearchTree<K extends Comparable<K>,V> implements Dictionary<K, V> {

    // This is the BST implementation, KeyValueHashTable has the hash table implementation

    private TreeNode root = null;
    private int count = 0;

    @Override
    public Type getType() {
       return Type.BST;
    }
 
    @Override
    public int size() {
        return count;
    }

    /**
     * Prints out the statistics of the tree structure usage.
     * Here you should print out member variable information which tell something about
     * your implementation.
     * <p>
     * For example, if you implement this using a hash table, update member variables of the class
     * (int counters) in add(K) whenever a collision happen. Then print this counter value here. 
     * You will then see if you have too many collisions. It will tell you that your hash function
     * is good or bad (too much collisions against data size).
     */
    @Override
    public String getStatus() {
        return "some bst status";
    }

    @Override
    public boolean add(K key, V value) throws IllegalArgumentException, OutOfMemoryError {
        if (null == key) throw new IllegalArgumentException("Key cannot be null");
        root = add(root, key, value);
        return true;
    }

    private TreeNode<K, V> add(TreeNode<K, V> node, K key, V value) {
        if (node == null) {
            count++;
            return new TreeNode<>(key, value);
        }
        // Compare the new key with the current node's key
        int cmp = key.compareTo(node.key);
        if (cmp < 0) { // If the new key is smaller, go to the left subtree
            node.left = add(node.left, key, value);
        } else if (cmp > 0) { // If the new key is larger, go to the right subtree
            node.right = add(node.right, key, value);
        } else { // If the keys are equal, update the value
            node.value = value;
        }
        return node;
    }

    @Override
    public V find(K key) throws IllegalArgumentException {
        if (null == key) throw new IllegalArgumentException("Key cannot be null");
        return (V) find(root, key);
    }

    public V find(TreeNode<K, V> node, K key) {
        if (node == null) {
            return null;
        }
        // Compare the new key with the current node's key
        int cmp = key.compareTo(node.key);
        if (cmp < 0) { // If the key is smaller, go to the left subtree
            return (V) find(node.left, key);
        } else if (cmp > 0) { // If the key is larger, go to the right subtree
            return (V) find(node.right, key);
        } else { // If the keys are equal, we found it
            return node.value;
        }
    }

        @Override
    public void ensureCapacity(int size) throws OutOfMemoryError {
        // not needed
    }

    @Override
    public Pair<K,V> [] toSortedArray() {
        Pair<K, V> [] pairs = new Pair[count];
        toSortedArray(root,pairs,0);
        return pairs;
      }

    public int toSortedArray(TreeNode<K, V> node, Pair<K, V> [] pairs, int index) {
        if (node==null) {
            return index;
        }
        int leftmost = toSortedArray(node.left, pairs, index);
        pairs[leftmost] = new Pair<>(node.key, node.value);
        int rightmost = toSortedArray(node.right, pairs, leftmost+1);
        return rightmost;
    }

      @Override
      public void compress() throws OutOfMemoryError {
        // not needed
    }

    private static class TreeNode<K extends Comparable<K>,V> {
        K key;
        V value;

        TreeNode left = null;
        TreeNode right = null;

        TreeNode(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}

