package oy.tol.tra;

/**
 * Represents a person with a first name and a last name.
 * Implements Comparable to provide a natural ordering based on full names.
 */
public class Person implements Comparable<Person> {
    private String firstName;
    private String lastName;

    /**
     * Constructor for creating a Person object with a first name and a last name.
     *
     * @param firstName The first name of the person.
     * @param lastName  The last name of the person.
     */
    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Gets the last name of the person.
     *
     * @return The last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Gets the first name of the person.
     *
     * @return The first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gets the full name of the person by concatenating last name and first name.
     *
     * @return The full name.
     */
    public String getFullName() {
        return lastName + " " + firstName;
    }

    /**
     * Calculates the hash code for the Person object.
     *
     * @return The hash code.
     */
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        return result;
    }

    /**
     * Compares two Person objects based on their full names.
     *
     * @param toValue The Person to compare with.
     * @return A negative integer, zero, or a positive integer as this person is less than, equal to,
     *         or greater than the specified person.
     */
    @Override
    public int compareTo(Person toValue) {
        String thisFullName = this.lastName + this.firstName;
        String toValueFullName = toValue.lastName + toValue.firstName;

        int i = 0;
        // Compare characters of full names until a difference is found or the end of one of the names is reached
        while (i < thisFullName.length() && i < toValueFullName.length()) {
            if (thisFullName.charAt(i) != toValueFullName.charAt(i)) {
                return thisFullName.charAt(i) - toValueFullName.charAt(i);
            }
            i++;
        }

        // If the characters are the same up to this point, compare the lengths
        return thisFullName.length() - toValueFullName.length();
    }

    /**
     * Checks if two Person objects are equal based on their first names and last names.
     *
     * @param toValue The object to compare with.
     * @return True if the objects are equal, false otherwise.
     */
    @Override
    public boolean equals(Object toValue) {
        // Check if the object is null or not an instance of Person
        if (toValue == null || !(toValue instanceof Person)) {
            return false;
        }

        Person persToValue = (Person) toValue;
        int i = 0;
        // Compare characters of first names until a difference is found or the end of one of the names is reached
        while (i < this.firstName.length() && i < persToValue.firstName.length()) {
            if (this.firstName.charAt(i) != persToValue.firstName.charAt(i)) {
                return false;
            }
            i++;
        }

        i = 0;
        // Compare characters of last names until a difference is found or the end of one of the names is reached
        while (i < this.lastName.length() && i < persToValue.lastName.length()) {
            if (this.lastName.charAt(i) != persToValue.lastName.charAt(i)) {
                return false;
            }
            i++;
        }

        // Check if the lengths of first names and last names are equal
        return this.firstName.length() == persToValue.firstName.length()
                && this.lastName.length() == persToValue.lastName.length();
    }
}
