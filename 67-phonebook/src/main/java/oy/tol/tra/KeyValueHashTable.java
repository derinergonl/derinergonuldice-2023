package oy.tol.tra;

import static java.lang.Math.abs;

/**
 * Represents a hash table that stores key-value pairs.
 *
 * @param <K> The type of keys.
 * @param <V> The type of values.
 */
public class KeyValueHashTable<K extends Comparable<K>, V> implements Dictionary<K, V> {
    private static final double MAX_LOAD_FACTOR = 0.75;
    private double loadFactor = 0.0;
    private int reallocationsCount = 0;
    private HashNode[] buckets = new HashNode[20];
    private int allCount = 0;

    /**
     * Constructs a KeyValueHashTable with a specified capacity.
     *
     * @param capacity The initial capacity of the hash table.
     * @throws OutOfMemoryError If there is not enough memory to allocate the hash table.
     */
    public KeyValueHashTable(int capacity) throws OutOfMemoryError {
        ensureCapacity(capacity);
    }

    /**
     * Constructs a KeyValueHashTable with a default capacity of 20.
     *
     * @throws OutOfMemoryError If there is not enough memory to allocate the hash table.
     */
    public KeyValueHashTable() throws OutOfMemoryError {
        ensureCapacity(20);
    }

    @Override
    public Type getType() {
        return Type.HASHTABLE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void ensureCapacity(int size) throws OutOfMemoryError {
        if (buckets.length < size) {
            buckets = new HashNode[size];
        }
    }

    @Override
    public int size() {
        return allCount;
    }

    @Override
    public String getStatus() {
        return "hashtable status: allCount=" + allCount + ", reallocationsCount=" + reallocationsCount + ", loadFactor=" + loadFactor;
    }

    @Override
    public boolean add(K key, V value) throws IllegalArgumentException, OutOfMemoryError {
        if (null == key || value == null) {
            throw new IllegalArgumentException("Neither Key nor value can be null");
        }
        boolean added = add(new Pair<>(key, value), buckets);
        if (added) {
            allCount++;
            loadFactor = (double) allCount / buckets.length;
            if (loadFactor > MAX_LOAD_FACTOR) {
                reallocateBuckets();
            }
            return true;
        }
        return false;
    }

    private static <K extends Comparable<K>, V> boolean add(Pair<K, V> pair, HashNode[] buckets) {
        int indexToAdd = rehash(pair.getKey().hashCode(), buckets.length);
        HashNode bucket = buckets[indexToAdd];
        if (bucket == null) {
            bucket = new HashNode();
            buckets[indexToAdd] = bucket;
        }

        // Replace if condition with while loop
        while (true) {
            if (bucket.add(pair)) {
                break;
            }
        }

        return true;
    }

    @Override
    public V find(K key) throws IllegalArgumentException {
        if (null == key) throw new IllegalArgumentException("Key cannot be null");

        int index = rehash(key.hashCode(), buckets.length);
        HashNode bucket = buckets[index];

        if (bucket == null) return null;
        else return (V) bucket.find(key);
    }

    @Override
    @java.lang.SuppressWarnings({"unchecked"})
    public Pair<K, V>[] toSortedArray() {
        Pair<K, V>[] sorted = (Pair<K, V>[]) new Pair[allCount];
        int newIndex = 0;
        for (int index = 0; index < buckets.length; index++) {
            HashNode bucket = buckets[index];
            if (bucket != null) {
                for (int j = 0; j < bucket.countPairs; j++) {
                    sorted[newIndex++] = bucket.pairs[j];
                }
            }
        }
        Algorithms.fastSort(sorted);
        return sorted;
    }

    @Override
    public void compress() throws OutOfMemoryError {
        //TODO
    }

    private void reallocateBuckets() throws OutOfMemoryError { //reallocate and rehash
        int newSize = (int) (allCount / 0.25);
        HashNode[] newBuckets = new HashNode[newSize];
        for (int index = 0; index < buckets.length; index++) {
            HashNode bucket = buckets[index];
            if (bucket != null) {
                for (int j = 0; j < bucket.countPairs; j++) {
                    add(bucket.pairs[j], newBuckets);
                }
            }
        }
        buckets = newBuckets;
        loadFactor = (double) allCount / buckets.length;
        reallocationsCount++;
    }

    private static int rehash(int hashCode, int capacity) {
        return abs(hashCode) % capacity;
    }

    private static class HashNode<K extends Comparable<K>, V> {
        private Pair<K, V>[] pairs = new Pair[2];
        private int countPairs = 0;

        V find(K key) {
            for (int i = 0; i < countPairs; i++) {
                if (pairs[i].getKey().equals(key)) {
                    return pairs[i].getValue();
                }
            }
            return null;
        }

        boolean add(Pair<K, V> pair) {
            if (countPairs >= pairs.length) {
                reallocatePairs(pairs.length * 2);
            }
            for (int i = 0; i < countPairs; i++) {
                // Replace if condition with while loop
                while (pairs[i].getKey() == pair.getKey()) {
                    pairs[i].setvalue(pair.getValue());
                    return false;
                }
            }
            pairs[countPairs] = pair;
            countPairs++;
            return true;
        }

        private void reallocatePairs(int newSize) throws OutOfMemoryError {
            Pair<K, V>[] newArray = new Pair[newSize];
            for (int index = 0; index < countPairs; index++) {
                newArray[index] = pairs[index];
            }
            pairs = newArray;
        }
    }
}
