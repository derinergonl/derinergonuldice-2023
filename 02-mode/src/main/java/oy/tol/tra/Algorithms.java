package oy.tol.tra;

public class Algorithms {

    // Container class for mode search results.
    public static class ModeSearchResult<T> {
        public T theMode;  // The most frequently occurring element in the array.
        public int count = 0;  // The count of occurrences for the mode.
    }

    // Swaps two elements in the array.
    public static <T> void swap(T[] array, int i, int j) {
        T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    // Sorts the array using the bubble sort algorithm.
    public static <T extends Comparable<T>> void sort(T[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    swap(array, j, j + 1);
                }
            }
        }
    }

    // Reverses the order of elements in the array.
    public static <T> void reverse(T[] array) {
        for (int i = 0, j = array.length - 1; i < j; i++, j--) {
            swap(array, i, j);
        }
    }

    // Finds the mode (most frequent element) in the sorted array.
    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T[] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();

        if (array == null || array.length <= 1) {
            result.theMode = null;  // No mode for empty or single-element array.
            result.count = -1;  // Invalid count for empty or single-element array.
            return result;
        }

        sort(array);

        T mode = array[0];
        int appearance = 1;
        int count = 1;

        for (int i = 1; i < array.length; i++) {
            if (array[i].equals(array[i - 1])) {
                count++;
            } else {
                if (count > appearance) {
                    mode = array[i - 1];
                    appearance = count;
                }
                count = 1;
            }
        }

        if (count > appearance) {
            mode = array[array.length - 1];
            appearance = count;
        }

        result.count = appearance;
        result.theMode = mode;

        return result;
    }
}

// Julia Lehto and i worked on this code together and inspired by each others work.//